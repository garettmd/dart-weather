# A Dart app using Dark Sky's Weather API

### Why?

To learn Dart

### Is it that useful?

Not really

### Does it work

Yes!

### Usage

```dart
dart bin/main.dart -z 73162

The weather for today:
	Partly cloudy overnight.
	High temp: 80.65
	Low temp: 62.41
```

