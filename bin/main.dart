import 'package:weather/weather.dart' as weather;
import 'package:args/args.dart';

ArgResults argResults;

void main(List<String> arguments) async {
  final parser = new ArgParser()..addOption('zip', abbr: 'z');
  argResults = parser.parse(arguments);
  String zip = argResults['zip'];

  var temps = await weather.weatherByZIP(zip);
  print('The weather for today:');
  print('\t${temps['summary']}');
  print('\tHigh temp: ${temps['hi']}');
  print('\tLow temp: ${temps['low']}');
}
