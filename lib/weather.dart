import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'dart:io';
import 'package:safe_config/safe_config.dart';

class ApplicationConfiguration extends Configuration {
  ApplicationConfiguration(String fileName) : super.fromFile(File(fileName));

  String dark_sky_secret;
  String dark_sky_url;
  String geocode_secret;
  String geocode_url;
}

var config = new ApplicationConfiguration('config.yml');

String dsSecretKey = config.dark_sky_secret;
String darkSkyURL = config.dark_sky_url;
String geoSecretKey = config.geocode_secret;
String geocodeURL = config.geocode_url;

Future<Map<String, dynamic>> weatherByZIP(String zip) async {
  // Get Lat and Long from Google Maps API
  var geoResp = await http
      .get(geocodeURL + '?q=${zip},USA&key=${geoSecretKey}&format=json');
  var jsonGeoResponse = convert.jsonDecode(geoResp.body);
  String lat = jsonGeoResponse[0]['lat'];
  String long = jsonGeoResponse[0]['lon'];

  // Get forecast from Dark Sky
  var dsResp = await http.get(darkSkyURL + '/$dsSecretKey/${lat},${long}');
  var jsonDSResponse = convert.jsonDecode(dsResp.body);
  String summary = jsonDSResponse['daily']['data'][0]['summary'];
  double highTemp = jsonDSResponse['daily']['data'][0]['temperatureHigh'];
  double lowTemp = jsonDSResponse['daily']['data'][0]['temperatureLow'];
  return {'hi': highTemp, 'low': lowTemp, 'summary': summary};
}
